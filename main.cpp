#include <iostream>
#include <string>
#include <queue>
#include <map>
#include <vector>
using std::cout;
using std::endl;
using std::map;
using std::priority_queue;
using std::vector;
using std::string;


class Node {
	public:
		int frecuency;
		Node* children[2];

		Node(int _frecuency, Node* child1 = NULL, Node* child2 = NULL): 
				frecuency(_frecuency) {
			children[0] = child1;
			children[1] = child2;
		}
		virtual bool is_leaf() {
			return false;
		}
		virtual char get_character() {
			return 0;
		}
};


class LeafNode: public Node {
	public:
		char character;

		LeafNode(char _character, int _frecuency): character(_character), Node(_frecuency) {

		}
		bool is_leaf() {
			return true;
		}
		char get_character() {
			return character;
		}
};


class CompareNodes {
public:
	bool operator()(Node* node1, Node* node2) {
		return node1->frecuency >= node2->frecuency;
	}
};


struct Code {
	char code;
	int len;

	Code(char _code = 0, int _len = 0): code(_code), len(_len) {

	}
};

map<char, Code> codes;

void create_codes(Node* node, int level = 0, char code = 0) {
	if (node->is_leaf()) {
		codes[node->get_character()] = Code(code, level);
		return;
	}
	// bit in pos level is already = 0
	create_codes(node->children[0], level+1, code);
	code |= (1<<level);	// set bit in pos level = 1
	create_codes(node->children[1], level+1, code);
}


int main() {
	string msg = "Eerie eyes seen near lake.";
	cout << "message: " << msg << endl;

	// count characters
	map<char, int> characters;
	for (int i=0; i<msg.size(); ++i) {
		char character = msg[i];
		if (characters.count(character)) {
			characters[character]++;
		}
		else {
			characters[character] = 1;
		}
	}

	// enqueueing nodes
	priority_queue<Node*, vector<Node*>, CompareNodes> nodes;
	map<char, int>::iterator it;
	for (it=characters.begin(); it!=characters.end(); it++) {
		Node* node = new LeafNode(it->first, it->second);
		nodes.push(node);
	}

	// building tree
	while (nodes.size()>1) {
		Node* node1 = nodes.top();
		nodes.pop();
		Node* node2 = nodes.top();
		nodes.pop();
		int new_frecuency = node1->frecuency + node2->frecuency;
		Node* new_node = new Node(new_frecuency, node1, node2);
		nodes.push(new_node);
	}

	// create codes
	create_codes(nodes.top());

	// encoding msg
	string encoded_msg = "";
	int bits_used = 0;
	for (int i=0; i<msg.size(); ++i) {
		char code = codes[msg[i]].code;
		int len = codes[msg[i]].len;
		if (bits_used == 0) {
			encoded_msg += '\0';
		}
		encoded_msg[encoded_msg.size()-1] |= (code<<bits_used);
		if (len <= 8-bits_used) {
			bits_used += len;
			bits_used = bits_used % 8;
		}
		else {
			int copied = 8-bits_used;
			encoded_msg += '\0';
			encoded_msg[encoded_msg.size()-1] |= (code>>copied);
			bits_used = len - copied;			
		}
	}

	// printing encoded
	cout << "encoded: ";
	for (int i=0; i<encoded_msg.size(); ++i) {
		for (int j=0; j<8; ++j) {
			bool cur_bit = encoded_msg[i] & (1<<j);
			cout << cur_bit;
		}
	}
	cout << endl;

	// decoding msg
	string decoded_msg;
	Node* cur_node = nodes.top();	// start in root
	for (int i=0; i<encoded_msg.size(); ++i) {
		for (int j=0; j<8; ++j) {
			bool cur_bit = encoded_msg[i] & (1<<j);
			cur_node = cur_node->children[cur_bit];
			if (cur_node->is_leaf() && decoded_msg.size() < msg.size()) {
				decoded_msg += cur_node->get_character();
				cur_node = nodes.top();
			}
		}
	}
	cout << "decoded: " << decoded_msg << endl;
	return 0;
}